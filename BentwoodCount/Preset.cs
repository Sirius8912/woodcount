﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modbus;
using Modbus.Device;

namespace BentwoodCount
{
    public partial class Preset : Form
    {
        public Preset()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Variables.threshold = double.Parse(threshold.Text);
            Variables.closeCircle = double.Parse(closeCircle.Text);
            Variables.skeletonLength = double.Parse(skeletonLength.Text);
            Variables.segmentSmooth = double.Parse(segmentSooth.Text);
            Variables.maxDist1 = double.Parse(MaxDist1.Text);
            Variables.maxDist2 = double.Parse(MaxDist2.Text);
            Variables.selectContours = double.Parse(selectContours.Text);
            Variables.groupDist = double.Parse(groupDist.Text);
            Variables.MeanRadiusVar = double.Parse(MeanRadiusVar.Text);

            Variables.alarmGap = int.Parse(AlarmSeconds.Text);
            Variables.alarmRounds = int.Parse(alarmRounds.Text);

            
            File.WriteAllText("Para (1).txt", Variables.threshold.ToString());
            File.WriteAllText("Para (2).txt", Variables.closeCircle.ToString());
            File.WriteAllText("Para (3).txt", Variables.skeletonLength.ToString());
            File.WriteAllText("Para (4).txt", Variables.segmentSmooth.ToString());
            File.WriteAllText("Para (5).txt", Variables.maxDist1.ToString());
            File.WriteAllText("Para (6).txt", Variables.maxDist2.ToString());
            File.WriteAllText("Para (7).txt", Variables.selectContours.ToString());
            File.WriteAllText("Para (8).txt", Variables.groupDist.ToString());
            File.WriteAllText("Para (9).txt", Variables.MeanRadiusVar.ToString());
            File.WriteAllText("Para (10).txt", Variables.alarmGap.ToString());
            File.WriteAllText("Para (11).txt", Variables.alarmRounds.ToString());
            


        }

        private async void RedLightTestButton_Click(object sender, EventArgs e)
        {
            if (RedLightTestButton.Text=="测试")
            {
                Variables.StopAlarm = false;
                RedLightTestButton.Text = "停止";
                await Task.Run(async ()=> await RedLightOpen());
                RedLightTestButton.Text = "测试";
            }
            else
            {
                RedLightTestButton.Text = "测试";
                Variables.StopAlarm = true;
            }
        }

        //private void RedLightTest()
        //{
        //    MethodInvoker TestRedLight = new MethodInvoker(RedLightOpen);
        //    this.BeginInvoke(TestRedLight);
        //}

        //private void YellowLightTest()
        //{
        //    MethodInvoker TestYellowLight = new MethodInvoker(YellowLightOpen);
        //    this.BeginInvoke(TestYellowLight);
        //}

        //private void GreebLightTest()
        //{
        //    MethodInvoker TestGreenLight = new MethodInvoker(GreenLightOpen);
        //    this.BeginInvoke(TestGreenLight);
        //}


        private void PLCControl(Action<IModbusMaster> action)
        {
            try
            {
                using (var port = new TcpClient())
                {
                    var master = Modbus.Device.ModbusIpMaster.CreateIp(port);

                    port.Connect("192.168.1.5", 502);//待修改
                    action(master);
                    port.Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"控制PLC出错，{ex.Message}");
            }
        }


        private async Task PLCControl(Func<IModbusMaster, Task> action)
        {
            try
            {
                using (var port = new TcpClient())
                {
                    var master = Modbus.Device.ModbusIpMaster.CreateIp(port);

                    port.Connect("192.168.1.5", 502);//待修改
                    await action(master);
                    port.Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"控制PLC出错，{ex.Message}");
            }
        }
        public async Task RedLightOpen()
        {
            await PLCControl(async master =>
            {
                byte slaveID = 1;//待修改

                for (int i = 1; i <= Variables.alarmRounds && !Variables.StopAlarm; i++)
                {
                    master.WriteSingleCoil(slaveID, 1280, true);
                    master.WriteSingleCoil(slaveID, 1281, false);
                    await Task.Delay((int)(Variables.alarmGap));
                    master.WriteSingleCoil(slaveID, 1280, false);
                    master.WriteSingleCoil(slaveID, 1281, true);
                    await Task.Delay((int)(Variables.alarmGap));
                }

                master.WriteSingleCoil(slaveID, 1280, false);
                master.WriteSingleCoil(slaveID, 1281, false);
            });
            //OPEN THE 1296-1327 PLC COIL

        }
        public void YellowLightOpen()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改
                master.WriteSingleCoil(slaveID, 1282, true);
            });
            //OPEN THE 1296-1327 PLC COIL

        }

        public void YellowLightClose()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改
                master.WriteSingleCoil(slaveID, 1282, false);
            });
            //OPEN THE 1296-1327 PLC COIL

        }

        public void GreenLightClose()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改
                master.WriteSingleCoil(slaveID, 1283, false);
            });
            //OPEN THE 1296-1327 PLC COIL

        }


        public void GreenLightOpen()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改
                master.WriteSingleCoil(slaveID, 1283, true);
            });
            //OPEN THE 1296-1327 PLC COIL

        }

        private void YellowLightTestButton_Click(object sender, EventArgs e)
        {
            if (YellowLightTestButton.Text == "测试")
            {
                YellowLightTestButton.Text = "停止";
                Task.Run(new Action(YellowLightOpen));
            }
            else
            {
                YellowLightTestButton.Text = "测试";
                Task.Run(new Action(YellowLightClose));
            }
        }


        public void WoodInSign()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改

                
                bool[] WoodIn = master.ReadInputs(slaveID, (ushort)1024, 7);


            });
            //OPEN THE 1296-1327 PLC COIL

        }
        private void GreenLightTestButton_Click(object sender, EventArgs e)
        {
            if (GreenLightTestButton.Text == "测试")
            {
                GreenLightTestButton.Text = "停止";
                Task.Run(new Action(GreenLightOpen));
            }
            else
            {
                GreenLightTestButton.Text = "测试";
                Task.Run(new Action(GreenLightClose));
            }
        }

        private void threshold_TextChanged(object sender, EventArgs e)
        {

        }

        private void closeCircle_TextChanged(object sender, EventArgs e)
        {

        }

        private void Preset_Load(object sender, EventArgs e)
        {
            threshold.Text=Variables.threshold.ToString();
            closeCircle.Text = Variables.closeCircle.ToString();
            skeletonLength.Text = Variables.skeletonLength.ToString();
            segmentSooth.Text= Variables.segmentSmooth.ToString();
            MaxDist1.Text=Variables.maxDist1.ToString();
            MaxDist2.Text=Variables.maxDist2.ToString();
            selectContours.Text= Variables.selectContours.ToString();
            groupDist.Text= Variables.groupDist.ToString();
            MeanRadiusVar.Text = Variables.MeanRadiusVar.ToString();
            AlarmSeconds.Text= Variables.alarmGap.ToString();
            alarmRounds.Text= Variables.alarmRounds.ToString();


        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
