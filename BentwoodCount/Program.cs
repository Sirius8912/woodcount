﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HalconDotNet;
using Modbus.Device;

namespace BentwoodCount
{
    static class InvokeHelper
    {
        public static void InvokeIfRequired(this MainForm ctl, Action action)
        {
            if (ctl.InvokeRequired)
            {
                ctl.Invoke(action);
            }
            else
            {
                action();
            }
        }
    }
    static class Variables
    {
        public static double threshold=190;
        public static double closeCircle=5.5;
        public static double skeletonLength=3;
        public static double segmentSmooth=5;
        public static double maxDist1=6;
        public static double maxDist2=3.5;
        public static double selectContours=10;
        public static double groupDist=100;
        public static double MeanRadiusVar = 150;
        public static int alarmGap=50;
        public static int StandardNum;
        public static int LeftStandardNum;
        public static int RightStandardNum;
        public static bool StopAlarm=false;
        public static int alarmRounds=4;
        public static int countNum=0;
        public static int countLeftNum=0;
        public static int countRightNum=0;
        public static bool WoodIn1;
        public static bool WoodIn2;
        public static bool WoodIn3;
        public static bool WoodIn4;
        public static bool WoodIn5;
        public static bool WoodIn6;

        public static bool Multi = false;
        public static bool rollback = false;
        public static bool pause = false;
    }

    static class Program
    {


        
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }



    public partial class HDevelopExport
    {


        public bool StartTag = false;
        public bool LeftTag = false;
        public bool RightTag = false;

        public HTuple hv_ExpDefaultWinHandle;

        public void HDevelopStop()
        {
            MessageBox.Show("Press button to continue", "Program stop");
        }

        // Procedures 
        // Chapter: Graphics / Text
        // Short Description: This procedure writes a text message. 
        public void disp_message(HTuple hv_WindowHandle, HTuple hv_String, HTuple hv_CoordSystem,
            HTuple hv_Row, HTuple hv_Column, HTuple hv_Color, HTuple hv_Box)
        {



            // Local iconic variables 

            // Local control variables 

            HTuple hv_GenParamName = null, hv_GenParamValue = null;
            HTuple hv_Color_COPY_INP_TMP = hv_Color.Clone();
            HTuple hv_Column_COPY_INP_TMP = hv_Column.Clone();
            HTuple hv_CoordSystem_COPY_INP_TMP = hv_CoordSystem.Clone();
            HTuple hv_Row_COPY_INP_TMP = hv_Row.Clone();

            // Initialize local and output iconic variables 
            //This procedure displays text in a graphics window.
            //
            //Input parameters:
            //WindowHandle: The WindowHandle of the graphics window, where
            //   the message should be displayed
            //String: A tuple of strings containing the text message to be displayed
            //CoordSystem: If set to 'window', the text position is given
            //   with respect to the window coordinate system.
            //   If set to 'image', image coordinates are used.
            //   (This may be useful in zoomed images.)
            //Row: The row coordinate of the desired text position
            //   A tuple of values is allowed to display text at different
            //   positions.
            //Column: The column coordinate of the desired text position
            //   A tuple of values is allowed to display text at different
            //   positions.
            //Color: defines the color of the text as string.
            //   If set to [], '' or 'auto' the currently set color is used.
            //   If a tuple of strings is passed, the colors are used cyclically...
            //   - if |Row| == |Column| == 1: for each new textline
            //   = else for each text position.
            //Box: If Box[0] is set to 'true', the text is written within an orange box.
            //     If set to' false', no box is displayed.
            //     If set to a color string (e.g. 'white', '#FF00CC', etc.),
            //       the text is written in a box of that color.
            //     An optional second value for Box (Box[1]) controls if a shadow is displayed:
            //       'true' -> display a shadow in a default color
            //       'false' -> display no shadow
            //       otherwise -> use given string as color string for the shadow color
            //
            //It is possible to display multiple text strings in a single call.
            //In this case, some restrictions apply:
            //- Multiple text positions can be defined by specifying a tuple
            //  with multiple Row and/or Column coordinates, i.e.:
            //  - |Row| == n, |Column| == n
            //  - |Row| == n, |Column| == 1
            //  - |Row| == 1, |Column| == n
            //- If |Row| == |Column| == 1,
            //  each element of String is display in a new textline.
            //- If multiple positions or specified, the number of Strings
            //  must match the number of positions, i.e.:
            //  - Either |String| == n (each string is displayed at the
            //                          corresponding position),
            //  - or     |String| == 1 (The string is displayed n times).
            //
            //
            //Convert the parameters for disp_text.
            if ((int)((new HTuple(hv_Row_COPY_INP_TMP.TupleEqual(new HTuple()))).TupleOr(
                new HTuple(hv_Column_COPY_INP_TMP.TupleEqual(new HTuple())))) != 0)
            {

                return;
            }
            if ((int)(new HTuple(hv_Row_COPY_INP_TMP.TupleEqual(-1))) != 0)
            {
                hv_Row_COPY_INP_TMP = 12;
            }
            if ((int)(new HTuple(hv_Column_COPY_INP_TMP.TupleEqual(-1))) != 0)
            {
                hv_Column_COPY_INP_TMP = 12;
            }
            //
            //Convert the parameter Box to generic parameters.
            hv_GenParamName = new HTuple();
            hv_GenParamValue = new HTuple();
            if ((int)(new HTuple((new HTuple(hv_Box.TupleLength())).TupleGreater(0))) != 0)
            {
                if ((int)(new HTuple(((hv_Box.TupleSelect(0))).TupleEqual("false"))) != 0)
                {
                    //Display no box
                    hv_GenParamName = hv_GenParamName.TupleConcat("box");
                    hv_GenParamValue = hv_GenParamValue.TupleConcat("false");
                }
                else if ((int)(new HTuple(((hv_Box.TupleSelect(0))).TupleNotEqual("true"))) != 0)
                {
                    //Set a color other than the default.
                    hv_GenParamName = hv_GenParamName.TupleConcat("box_color");
                    hv_GenParamValue = hv_GenParamValue.TupleConcat(hv_Box.TupleSelect(0));
                }
            }
            if ((int)(new HTuple((new HTuple(hv_Box.TupleLength())).TupleGreater(1))) != 0)
            {
                if ((int)(new HTuple(((hv_Box.TupleSelect(1))).TupleEqual("false"))) != 0)
                {
                    //Display no shadow.
                    hv_GenParamName = hv_GenParamName.TupleConcat("shadow");
                    hv_GenParamValue = hv_GenParamValue.TupleConcat("false");
                }
                else if ((int)(new HTuple(((hv_Box.TupleSelect(1))).TupleNotEqual("true"))) != 0)
                {
                    //Set a shadow color other than the default.
                    hv_GenParamName = hv_GenParamName.TupleConcat("shadow_color");
                    hv_GenParamValue = hv_GenParamValue.TupleConcat(hv_Box.TupleSelect(1));
                }
            }
            //Restore default CoordSystem behavior.
            if ((int)(new HTuple(hv_CoordSystem_COPY_INP_TMP.TupleNotEqual("window"))) != 0)
            {
                hv_CoordSystem_COPY_INP_TMP = "image";
            }
            //
            if ((int)(new HTuple(hv_Color_COPY_INP_TMP.TupleEqual(""))) != 0)
            {
                //disp_text does not accept an empty string for Color.
                hv_Color_COPY_INP_TMP = new HTuple();
            }
            //
            HOperatorSet.DispText(hv_ExpDefaultWinHandle, hv_String, hv_CoordSystem_COPY_INP_TMP,
                hv_Row_COPY_INP_TMP, hv_Column_COPY_INP_TMP, hv_Color_COPY_INP_TMP, hv_GenParamName,
                hv_GenParamValue);

            return;
        }

          public void set_display_font (HTuple hv_WindowHandle, HTuple hv_Size, HTuple hv_Font, 
      HTuple hv_Bold, HTuple hv_Slant)
  {



    // Local iconic variables 

    // Local control variables 

    HTuple hv_OS = null, hv_Fonts = new HTuple();
    HTuple hv_Style = null, hv_Exception = new HTuple(), hv_AvailableFonts = null;
    HTuple hv_Fdx = null, hv_Indices = new HTuple();
    HTuple   hv_Font_COPY_INP_TMP = hv_Font.Clone();
    HTuple   hv_Size_COPY_INP_TMP = hv_Size.Clone();

    // Initialize local and output iconic variables 
    //This procedure sets the text font of the current window with
    //the specified attributes.
    //
    //Input parameters:
    //WindowHandle: The graphics window for which the font will be set
    //Size: The font size. If Size=-1, the default of 16 is used.
    //Bold: If set to 'true', a bold font is used
    //Slant: If set to 'true', a slanted font is used
    //
    HOperatorSet.GetSystem("operating_system", out hv_OS);
    // dev_get_preferences(...); only in hdevelop
    // dev_set_preferences(...); only in hdevelop
    if ((int)((new HTuple(hv_Size_COPY_INP_TMP.TupleEqual(new HTuple()))).TupleOr(
        new HTuple(hv_Size_COPY_INP_TMP.TupleEqual(-1)))) != 0)
    {
      hv_Size_COPY_INP_TMP = 16;
    }
    if ((int)(new HTuple(((hv_OS.TupleSubstr(0,2))).TupleEqual("Win"))) != 0)
    {
      //Restore previous behaviour
      hv_Size_COPY_INP_TMP = ((1.13677*hv_Size_COPY_INP_TMP)).TupleInt();
    }
    else
    {
      hv_Size_COPY_INP_TMP = hv_Size_COPY_INP_TMP.TupleInt();
    }
    if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("Courier"))) != 0)
    {
      hv_Fonts = new HTuple();
      hv_Fonts[0] = "Courier";
      hv_Fonts[1] = "Courier 10 Pitch";
      hv_Fonts[2] = "Courier New";
      hv_Fonts[3] = "CourierNew";
      hv_Fonts[4] = "Liberation Mono";
    }
    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("mono"))) != 0)
    {
      hv_Fonts = new HTuple();
      hv_Fonts[0] = "Consolas";
      hv_Fonts[1] = "Menlo";
      hv_Fonts[2] = "Courier";
      hv_Fonts[3] = "Courier 10 Pitch";
      hv_Fonts[4] = "FreeMono";
      hv_Fonts[5] = "Liberation Mono";
    }
    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("sans"))) != 0)
    {
      hv_Fonts = new HTuple();
      hv_Fonts[0] = "Luxi Sans";
      hv_Fonts[1] = "DejaVu Sans";
      hv_Fonts[2] = "FreeSans";
      hv_Fonts[3] = "Arial";
      hv_Fonts[4] = "Liberation Sans";
    }
    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("serif"))) != 0)
    {
      hv_Fonts = new HTuple();
      hv_Fonts[0] = "Times New Roman";
      hv_Fonts[1] = "Luxi Serif";
      hv_Fonts[2] = "DejaVu Serif";
      hv_Fonts[3] = "FreeSerif";
      hv_Fonts[4] = "Utopia";
      hv_Fonts[5] = "Liberation Serif";
    }
    else
    {
      hv_Fonts = hv_Font_COPY_INP_TMP.Clone();
    }
    hv_Style = "";
    if ((int)(new HTuple(hv_Bold.TupleEqual("true"))) != 0)
    {
      hv_Style = hv_Style+"Bold";
    }
    else if ((int)(new HTuple(hv_Bold.TupleNotEqual("false"))) != 0)
    {
      hv_Exception = "Wrong value of control parameter Bold";
      throw new HalconException(hv_Exception);
    }
    if ((int)(new HTuple(hv_Slant.TupleEqual("true"))) != 0)
    {
      hv_Style = hv_Style+"Italic";
    }
    else if ((int)(new HTuple(hv_Slant.TupleNotEqual("false"))) != 0)
    {
      hv_Exception = "Wrong value of control parameter Slant";
      throw new HalconException(hv_Exception);
    }
    if ((int)(new HTuple(hv_Style.TupleEqual(""))) != 0)
    {
      hv_Style = "Normal";
    }
    HOperatorSet.QueryFont(hv_ExpDefaultWinHandle, out hv_AvailableFonts);
    hv_Font_COPY_INP_TMP = "";
    for (hv_Fdx=0; (int)hv_Fdx<=(int)((new HTuple(hv_Fonts.TupleLength()))-1); hv_Fdx = (int)hv_Fdx + 1)
    {
      hv_Indices = hv_AvailableFonts.TupleFind(hv_Fonts.TupleSelect(hv_Fdx));
      if ((int)(new HTuple((new HTuple(hv_Indices.TupleLength())).TupleGreater(0))) != 0)
      {
        if ((int)(new HTuple(((hv_Indices.TupleSelect(0))).TupleGreaterEqual(0))) != 0)
        {
          hv_Font_COPY_INP_TMP = hv_Fonts.TupleSelect(hv_Fdx);
          break;
        }
      }
    }
    if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual(""))) != 0)
    {
      throw new HalconException("Wrong value of control parameter Font");
    }
    hv_Font_COPY_INP_TMP = (((hv_Font_COPY_INP_TMP+"-")+hv_Style)+"-")+hv_Size_COPY_INP_TMP;
    HOperatorSet.SetFont(hv_ExpDefaultWinHandle, hv_Font_COPY_INP_TMP);
    // dev_set_preferences(...); only in hdevelop

    return;
  }
        // Main procedure 
        private async void action()
        {

            int maxCountNum = 0;
            int maxLeftNum = 0;
            int maxRightNum = 0;

            // Stack for temporary objects 
            HObject[] OTemp = new HObject[20];

            // Local iconic variables 

            HObject ho_Image = null, ho_ImageMean = null, ho_RegionDynThresh = null;
            HObject ho_ConnectedRegions = null, ho_SelectedRegions = null;
            HObject ho_RegionFillUp = null, ho_RegionFillUp1 = null, ho_Region = null;
            HObject ho_Region1 = null, ho_Skeleton = null, ho_Contours = null;
            HObject ho_ContoursSplit = null, ho_SelectedContours = null;
            HObject ho_SortedContours = null, ho_ObjectSelected = null;
            HObject ho_ContCircle = null;

            // Local control variables 

            HTuple hv_fileNo = new HTuple(), hv_AcqHandle = new HTuple();
            HTuple hv_T1 = new HTuple(), hv_countLayer = new HTuple();
            HTuple hv_rawRadius = new HTuple(), hv_rawRows = new HTuple();
            HTuple hv_rawCenter = new HTuple(), hv_rawColumns = new HTuple();
            HTuple hv_secondRows = new HTuple(), hv_secondColumns = new HTuple();
            HTuple hv_LayerRowList = new HTuple(), hv_LayerColumnList = new HTuple();
            HTuple hv_group1Num = new HTuple(), hv_group2Num = new HTuple();
            HTuple hv_maxNum = new HTuple();
            HTuple hv_maxLeftNum = new HTuple();
            HTuple hv_maxRightNum = new HTuple();
            HTuple hv_groupJump = new HTuple(), hv_averageDistance = new HTuple();
            HTuple hv_T2 = new HTuple(), hv_Width = new HTuple(), hv_Height = new HTuple();
            HTuple hv_WindowHandle = new HTuple(), hv_Number = new HTuple();
            HTuple hv_T3 = new HTuple(), hv_meanradius = new HTuple();
            HTuple hv_circles = new HTuple(), hv_K = new HTuple();
            HTuple hv_Attrib = new HTuple(), hv_Row = new HTuple();
            HTuple hv_Column = new HTuple(), hv_Radius = new HTuple();
            HTuple hv_StartPhi = new HTuple(), hv_EndPhi = new HTuple();
            HTuple hv_PointOrder = new HTuple(), hv_secondcircles = new HTuple();
            HTuple hv_L = new HTuple(), hv_secondRadius = new HTuple();
            HTuple hv_I = new HTuple(), hv_jump = new HTuple(), hv_J = new HTuple();
            HTuple hv_Distance = new HTuple(), hv_SortedLayerColumn = new HTuple();
            HTuple hv_i = new HTuple(), hv_T4 = new HTuple();
            // Initialize local and output iconic variables 
            HOperatorSet.GenEmptyObj(out ho_Image);
            HOperatorSet.GenEmptyObj(out ho_ImageMean);
            HOperatorSet.GenEmptyObj(out ho_RegionDynThresh);
            HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
            HOperatorSet.GenEmptyObj(out ho_SelectedRegions);
            HOperatorSet.GenEmptyObj(out ho_RegionFillUp);
            HOperatorSet.GenEmptyObj(out ho_RegionFillUp1);
            HOperatorSet.GenEmptyObj(out ho_Region);
            HOperatorSet.GenEmptyObj(out ho_Region1);
            HOperatorSet.GenEmptyObj(out ho_Skeleton);
            HOperatorSet.GenEmptyObj(out ho_Contours);
            HOperatorSet.GenEmptyObj(out ho_ContoursSplit);
            HOperatorSet.GenEmptyObj(out ho_SelectedContours);
            HOperatorSet.GenEmptyObj(out ho_SortedContours);
            HOperatorSet.GenEmptyObj(out ho_ObjectSelected);
            HOperatorSet.GenEmptyObj(out ho_ContCircle);
            //Image Acquisition 01: Code generated by Image Acquisition 01

            //   Robust circle fitting
            //
            //fileNo := 000

            //Image Acquisition 01: Code generated by Image Acquisition 01



            //temp delete because of offlinetest

            //for (int i = 0;i < 20;i++)
            //Variables.countNum++;

            //

            
            HOperatorSet.OpenFramegrabber("GigEVision2", 0, 0, 0, 0, 0, 0, "progressive",
    -1, "default", -1, "false", "default", "9c14636ef413_DahuaTechnologyET0B_A3A20MG8BQ4",
    0, -1, out hv_AcqHandle);
            HOperatorSet.GrabImageStart(hv_AcqHandle, -1);
            
           
            var port = new TcpClient();
            var master= Modbus.Device.ModbusIpMaster.CreateIp(port);


            port.Connect("192.168.1.5", 502);//待修改


            bool[] WoodIn = master.ReadInputs(1, (ushort)1024, 6);


            while (true)
            {
                while (Variables.pause == false)
                {

                    //display zoom
                    set_display_font(hv_ExpDefaultWinHandle, 32, "mono", "true", "false");
                    disp_message(hv_ExpDefaultWinHandle, "曲木板层数探测系统",
"window", 15, 450, "blue", "true");
                    set_display_font(hv_ExpDefaultWinHandle, 20, "mono", "true", "false");



                    disp_message(hv_ExpDefaultWinHandle, "图像读取耗时:", "window", 550, 70, "black", "true");
                    disp_message(hv_ExpDefaultWinHandle, "预处理耗时:" , "window", 550, 750, "black", "true");
                    disp_message(hv_ExpDefaultWinHandle, "辨识处理耗时:", "window", 590, 70, "black", "true");
                    disp_message(hv_ExpDefaultWinHandle, "图像     " + "总耗时:", "window", 590, 750, "black", "true");

                    set_display_font(hv_ExpDefaultWinHandle, 30, "mono", "true", "false");

                    if (Variables.Multi == false)
                    {
                        disp_message(hv_ExpDefaultWinHandle, "曲木板实时探测层数:",
    "window", 120, 450, "black", "true");
                        disp_message(hv_ExpDefaultWinHandle, "曲木板结果层数:" ,
"window", 190, 450, "black", "true");


                    }
                    else
                    {
                        disp_message(hv_ExpDefaultWinHandle, "左通道实时层数:" ,
    "window", 360, 70, "black", "true");

                        disp_message(hv_ExpDefaultWinHandle, "右通道实时层数:" ,
                            "window", 360, 750, "black", "true");
                        disp_message(hv_ExpDefaultWinHandle, "左通道结果层数:" ,
"window", 410, 70, "black", "true");
                        disp_message(hv_ExpDefaultWinHandle, "右通道结果层数:" ,
"window", 410, 750, "black", "true");

                    }



                if(port.Connected)
                {
                WoodIn = master.ReadInputs(1, (ushort)1024, 6);
                Variables.WoodIn1 = WoodIn[0];
                Variables.WoodIn2 = WoodIn[1];
                Variables.WoodIn3 = WoodIn[2];
                Variables.WoodIn4 = WoodIn[3];
                Variables.WoodIn5 = WoodIn[4];
                Variables.WoodIn6 = WoodIn[5];

                }
                else
                {
                    MessageBox.Show("PLC失去连接，光电信号未获取");
                }


                if (Variables.Multi==true)
                {
                    if((Variables.WoodIn3 == true) || (Variables.WoodIn2 == true))
                        {
                        StartTag = true;
                            maxLeftNum = 0;
                            hv_maxLeftNum = 0;
                            Variables.countLeftNum = 0;
                            maxRightNum = 0;
                            hv_maxRightNum = 0;
                            Variables.countRightNum = 0;
                        }
                }
                else
                {
                    if ((Variables.WoodIn1 == true)|| (Variables.WoodIn2 == true) || (Variables.WoodIn3 == true))
                    {
                        StartTag = true;
                            maxCountNum = 0;
                            hv_maxNum = 0;
                            Variables.countNum = 0;
                    }
                }
                if (StartTag==true)
                {
                    hv_fileNo = 0;

                    while ((int)(1) != 0)
                    {
                        if(Variables.Multi==true)
                        {
                            if(Variables.WoodIn3==true)
                            {
                                LeftTag = true;


                                }


                            if (Variables.WoodIn2 == true)
                            {
                                RightTag = true;

                                }


                        }
                        HOperatorSet.CountSeconds(out hv_T1);
                        hv_countLayer = 0;
                        hv_rawRadius = new HTuple();
                        hv_rawRows = new HTuple();
                        hv_rawCenter = new HTuple();
                        hv_rawColumns = new HTuple();
                        hv_secondRows = new HTuple();
                        hv_secondColumns = new HTuple();
                        hv_LayerRowList = new HTuple();
                        hv_LayerColumnList = new HTuple();
                        hv_group1Num = 0;
                        hv_group2Num = 0;
                        hv_groupJump = 0;
                        hv_averageDistance = 0;
                        //dev_close_window(...);

                        ho_Image.Dispose();
                        HOperatorSet.GrabImageAsync(out ho_Image, hv_AcqHandle, -1);

                        HOperatorSet.CountSeconds(out hv_T2);
                        HOperatorSet.GetImageSize(ho_Image, out hv_Width, out hv_Height);
                        HOperatorSet.SetPart(hv_ExpDefaultWinHandle, 0, 0, hv_Height - 1, hv_Width - 1);
                        HOperatorSet.DispObj(ho_Image, hv_ExpDefaultWinHandle);
                        //dev_open_window(...);
                        ho_ImageMean.Dispose();
                        HOperatorSet.MeanImage(ho_Image, out ho_ImageMean, 5, 5);
                        ho_RegionDynThresh.Dispose();
                        HOperatorSet.DynThreshold(ho_Image, ho_ImageMean, out ho_RegionDynThresh,
                            3, "light");
                        ho_ConnectedRegions.Dispose();
                        HOperatorSet.Connection(ho_RegionDynThresh, out ho_ConnectedRegions);
                        ho_SelectedRegions.Dispose();
                        HOperatorSet.SelectShape(ho_ConnectedRegions, out ho_SelectedRegions, "area",
                            "and", 80, 99999);
                        ho_RegionFillUp.Dispose();
                        HOperatorSet.FillUp(ho_SelectedRegions, out ho_RegionFillUp);
                        ho_RegionFillUp1.Dispose();
                        HOperatorSet.FillUp(ho_RegionFillUp, out ho_RegionFillUp1);
                        ho_Region.Dispose();
                        HOperatorSet.FillUp(ho_RegionFillUp1, out ho_Region);

                        ho_Region1.Dispose();
                        HOperatorSet.Threshold(ho_Image, out ho_Region1, Variables.threshold, 255);
                        //closing_circle (Region1, Region, 13)
                        {
                            HObject ExpTmpOutVar_0;
                            HOperatorSet.FillUp(ho_Region1, out ExpTmpOutVar_0);
                            ho_Region1.Dispose();
                            ho_Region1 = ExpTmpOutVar_0;
                        }
                        {
                            HObject ExpTmpOutVar_0;
                            HOperatorSet.ClosingCircle(ho_Region1, out ExpTmpOutVar_0, Variables.closeCircle);
                            ho_Region1.Dispose();
                            ho_Region1 = ExpTmpOutVar_0;
                        }

                        ho_Skeleton.Dispose();
                        HOperatorSet.Skeleton(ho_Region1, out ho_Skeleton);

                        //erosion_circle (Region, RegionErosion, 1.5)
                        //gen_contour_region_xld (Skeleton, Contour, 'center')
                        ho_Contours.Dispose();
                        HOperatorSet.GenContoursSkeletonXld(ho_Skeleton, out ho_Contours, Variables.skeletonLength, "filter");

                        HOperatorSet.DispObj(ho_Image, hv_ExpDefaultWinHandle);
                        ho_ContoursSplit.Dispose();
                        HOperatorSet.SegmentContoursXld(ho_Contours, out ho_ContoursSplit, "lines_circles",
                            Variables.segmentSmooth, Variables.maxDist1, Variables.maxDist2);
                        //union_cocircular_contours_xld (ContoursSplit, UnionContours, 1.6, 3.14, 0.9, 0, 20, 20, 'true', 10)
                        ho_SelectedContours.Dispose();
                        HOperatorSet.SelectContoursXld(ho_ContoursSplit, out ho_SelectedContours,
                            "contour_length", Variables.selectContours, 9999, -0.5, 0.5);
                        HOperatorSet.CountObj(ho_SelectedContours, out hv_Number);
                        //dev_display (Image)
                        HOperatorSet.SetDraw(hv_ExpDefaultWinHandle, "margin");
                        HOperatorSet.SetColor(hv_ExpDefaultWinHandle, "white");
                        // dev_update_window(...); only in hdevelop
                        HOperatorSet.CountSeconds(out hv_T3);
                        hv_meanradius = 0;
                        hv_circles = 0;

                        ho_SortedContours.Dispose();
                        HOperatorSet.SortContoursXld(ho_SelectedContours, out ho_SortedContours,
                            "upper_left", "true", "row");

                        HTuple end_val75 = hv_Number;
                        HTuple step_val75 = 1;
                        for (hv_K = 1; hv_K.Continue(end_val75, step_val75); hv_K = hv_K.TupleAdd(step_val75))
                        {
                            ho_ObjectSelected.Dispose();
                            HOperatorSet.SelectObj(ho_SortedContours, out ho_ObjectSelected, hv_K);

                            HOperatorSet.GetContourGlobalAttribXld(ho_ObjectSelected, "cont_approx",
                                out hv_Attrib);
                            //Fit a circle to the line segment that are arcs of a circle
                            if ((int)(new HTuple(hv_Attrib.TupleGreater(0))) != 0)
                            {
                                HOperatorSet.FitCircleContourXld(ho_ObjectSelected, "atukey", -1, 2,
                                    0, 3, 2, out hv_Row, out hv_Column, out hv_Radius, out hv_StartPhi,
                                    out hv_EndPhi, out hv_PointOrder);
                                if ((int)(new HTuple(hv_Radius.TupleGreater(0))) != 0)
                                {

                                    hv_meanradius = hv_meanradius + hv_Radius;
                                    if (hv_rawRows == null)
                                        hv_rawRows = new HTuple();
                                    hv_rawRows[hv_circles] = hv_Row;
                                    if (hv_rawColumns == null)
                                        hv_rawColumns = new HTuple();
                                    hv_rawColumns[hv_circles] = hv_Column;
                                    if (hv_rawRadius == null)
                                        hv_rawRadius = new HTuple();
                                    hv_rawRadius[hv_circles] = hv_Radius;

                                    hv_circles = hv_circles + 1;
                                }

                            }
                        }

                        if ((int)(new HTuple(hv_circles.TupleEqual(0))) != 0)
                        {
                            break;
                        }

                        hv_meanradius = hv_meanradius / hv_circles;

                        hv_secondcircles = 0;

                        HTuple end_val103 = hv_circles;
                        HTuple step_val103 = 1;
                        for (hv_L = 1; hv_L.Continue(end_val103, step_val103); hv_L = hv_L.TupleAdd(step_val103))
                        {

                            if ((int)(new HTuple((((((hv_rawRadius.TupleSelect(hv_L - 1)) - hv_meanradius)).TupleAbs()
                                )).TupleLess(Variables.MeanRadiusVar))) != 0)
                            {

                                if (hv_secondRows == null)
                                    hv_secondRows = new HTuple();
                                hv_secondRows[hv_secondcircles] = hv_rawRows.TupleSelect(hv_L - 1);
                                if (hv_secondColumns == null)
                                    hv_secondColumns = new HTuple();
                                hv_secondColumns[hv_secondcircles] = hv_rawColumns.TupleSelect(hv_L - 1);
                                if (hv_secondRadius == null)
                                    hv_secondRadius = new HTuple();
                                hv_secondRadius[hv_secondcircles] = hv_rawRadius.TupleSelect(hv_L - 1);
                                hv_secondcircles = hv_secondcircles + 1;

                            }
                        }


                        HTuple end_val116 = hv_secondcircles;
                        HTuple step_val116 = 1;
                        for (hv_I = 1; hv_I.Continue(end_val116, step_val116); hv_I = hv_I.TupleAdd(step_val116))
                        {

                            //count_seconds (TT1)

                            hv_jump = 0;
                            HTuple end_val121 = 0;
                            HTuple step_val121 = -1;
                            for (hv_J = hv_countLayer - 1; hv_J.Continue(end_val121, step_val121); hv_J = hv_J.TupleAdd(step_val121))
                            {
                                //count_seconds (TT3)
                                if ((int)(new HTuple(hv_countLayer.TupleNotEqual(0))) != 0)
                                {

                                    HOperatorSet.DistancePp(hv_secondRows.TupleSelect(hv_I - 1), hv_secondColumns.TupleSelect(
                                        hv_I - 1), hv_LayerRowList.TupleSelect(hv_J), hv_LayerColumnList.TupleSelect(
                                        hv_J), out hv_Distance);
                                    //Distance := sqrt( (secondRows[I-1]-LayerRowList[J])*(secondRows[I-1]-LayerRowList[J])+(secondColumns[I-1]-LayerColumnList[J])*(secondColumns[I-1]-LayerColumnList[J]))

                                    if ((int)(new HTuple(hv_Distance.TupleLess(20))) != 0)
                                    {
                                        hv_jump = 1;
                                        //skip this circle
                                        break;
                                    }


                                }

                                //count_seconds (TT4)

                                //disp_message (WindowHandle, '内圈单轮测试耗时:'+ ((TT4 - TT3)*1000 )$'.6' + ' ms', 'window', 180, 20, 'black', 'true')

                                //wait_seconds (1)
                            }


                            //stop ()

                            if ((int)(new HTuple(hv_jump.TupleEqual(0))) != 0)
                            {

                                if (hv_LayerRowList == null)
                                    hv_LayerRowList = new HTuple();
                                hv_LayerRowList[hv_countLayer] = hv_secondRows.TupleSelect(hv_I - 1);
                                if (hv_LayerColumnList == null)
                                    hv_LayerColumnList = new HTuple();
                                hv_LayerColumnList[hv_countLayer] = hv_secondColumns.TupleSelect(hv_I - 1);
                                hv_countLayer = hv_countLayer + 1;

                                ho_ContCircle.Dispose();
                                HOperatorSet.GenCircleContourXld(out ho_ContCircle, hv_secondRows.TupleSelect(
                                    hv_I - 1), hv_secondColumns.TupleSelect(hv_I - 1), hv_secondRadius.TupleSelect(
                                    hv_I - 1), 0, (new HTuple(360)).TupleRad(), "positive", 1.0);
                                HOperatorSet.DispObj(ho_ContCircle, hv_ExpDefaultWinHandle);

                            }





                            //count_seconds (TT2)

                            //disp_message (WindowHandle, '外圈循环耗时:'+ ((TT2 - TT1)*1000 )$'.6' + ' ms', 'window', 200, 20, 'black', 'true')

                            //wait_seconds (1)


                        }

                                HOperatorSet.TupleSort(hv_LayerColumnList, out hv_SortedLayerColumn);

                            if ((int)(new HTuple(hv_countLayer.TupleEqual(0))) != 0)
                            {

                                hv_group1Num = 0;
                                hv_group2Num = 0;
                            }
                            else
                            {
                                if ((int)(new HTuple(((hv_SortedLayerColumn.TupleSelect(0))).TupleGreater(
hv_Width / 2))) != 0)
                                {
                                    hv_group1Num = 0;
                                    hv_group2Num = hv_countLayer.Clone();
                                }
                                else
                                {

                                    hv_group1Num = 1;
                                    HTuple end_val174 = hv_countLayer;
                                    HTuple step_val174 = 1;
                                    for (hv_i = 2; hv_i.Continue(end_val174, step_val174); hv_i = hv_i.TupleAdd(step_val174))
                                    {

                                        if ((int)(new HTuple(hv_i.TupleEqual(2))) != 0)
                                        {
                                            hv_averageDistance = (hv_SortedLayerColumn.TupleSelect(hv_i - 1)) - (hv_SortedLayerColumn.TupleSelect(
                                                hv_i - 2));
                                            hv_group1Num = hv_group1Num + 1;
                                        }
                                        else
                                        {

                                            if ((int)(new HTuple(((((hv_SortedLayerColumn.TupleSelect(hv_i - 1)) - (hv_SortedLayerColumn.TupleSelect(
                                                hv_i - 2))) - hv_averageDistance)).TupleGreater(Variables.groupDist))) != 0)
                                            {
                                                hv_groupJump = 1;
                                                hv_group2Num = hv_countLayer - hv_group1Num;
                                                break;
                                            }
                                            else
                                            {
                                                hv_averageDistance = (((hv_SortedLayerColumn.TupleSelect(hv_i - 1)) - (hv_SortedLayerColumn.TupleSelect(
                                                    hv_i - 2))) + hv_averageDistance) / 2;
                                                hv_group1Num = hv_group1Num + 1;
                                            }


                                        }



                                    }

                                }
                            }



                        HOperatorSet.CountSeconds(out hv_T4);

                            set_display_font(hv_ExpDefaultWinHandle, 30, "mono", "true", "false");
                            disp_message(hv_ExpDefaultWinHandle, "曲木板实时探测层数:" + hv_countLayer,
                            "window", 120, 410, "black", "true");

                if(port.Connected)
                {
                WoodIn = master.ReadInputs(1, (ushort)1024, 6);
                Variables.WoodIn1 = WoodIn[0];
                Variables.WoodIn2 = WoodIn[1];
                Variables.WoodIn3 = WoodIn[2];
                Variables.WoodIn4 = WoodIn[3];
                Variables.WoodIn5 = WoodIn[4];
                Variables.WoodIn6 = WoodIn[5];

                }
                else
                {
                    MessageBox.Show("PLC失去连接，光电信号未获取");
                }

                            try
                            {
                                if (Variables.Multi == false)

                                {
                                    if (maxCountNum == 0)
                                        maxCountNum = (int)hv_countLayer;
                                    else
                                    {
                                        if ((int)hv_countLayer > maxCountNum)
                                        {
                                            maxCountNum = (int)hv_countLayer;
                                        }
                                    }

                                    hv_maxNum = maxCountNum;


                                    if (StartTag == true && ((Variables.WoodIn1 == false) && (Variables.WoodIn2 == false) && (Variables.WoodIn3 == false)))
                                    {
                                        StartTag = false;
                                        Variables.countNum = maxCountNum;

                                        if (maxCountNum == Variables.StandardNum)
                                        {
                                            disp_message(hv_ExpDefaultWinHandle, "曲木板结果层数:" + hv_maxNum,
        "window", 190, 410, "black", "true");
                                            disp_message(hv_ExpDefaultWinHandle, "合格",
            "window", 190, 750, "black", "true");
                                        }
                                        else
                                        {
                                            disp_message(hv_ExpDefaultWinHandle, "曲木板结果层数:" + hv_maxNum,
        "window", 190, 410, "black", "true");
                                            disp_message(hv_ExpDefaultWinHandle, "不合格",
            "window", 190, 750, "red", "true");
                                            await Task.Run(async () => await RedLightOpen());
                                        }

                                        maxCountNum = 0;
                                        hv_maxNum = 0;
                                        Variables.countNum = 0;

                                    }
                                }

                                else
                                {
                                    disp_message(hv_ExpDefaultWinHandle, "左通道实时层数:" + hv_group1Num,
                                        "window", 360, 70, "black", "true");

                                    disp_message(hv_ExpDefaultWinHandle, "右通道实时层数:" + hv_group2Num,
                                        "window", 360, 800, "black", "true");
                                    if (LeftTag == true)
                                    {
                                        if (maxLeftNum == 0)
                                        {
                                            maxLeftNum = (int)hv_group1Num;

                                        }
                                        else
                                        {
                                            if ((int)hv_group1Num > maxLeftNum)
                                            {
                                                maxLeftNum = (int)hv_group1Num;
                                            }

                                        }

                                        hv_maxLeftNum = maxLeftNum;
                                        disp_message(hv_ExpDefaultWinHandle, "左通道结果层数:" + hv_maxLeftNum,
                "window", 410, 70, "black", "true");

                                        if (LeftTag == true && Variables.WoodIn3 == false)
                                        {
                                            Variables.countLeftNum = maxLeftNum;
                                            if (maxLeftNum == Variables.LeftStandardNum)
                                            {
                                                disp_message(hv_ExpDefaultWinHandle, "合格-左",
                "window", 460, 70, "black", "true");
                                            }
                                            else
                                            {
                                                disp_message(hv_ExpDefaultWinHandle, "不合格-左",
                "window", 460, 70, "red", "true");
                                                await Task.Run(async () => await RedLightOpen());
                                            }

                                            maxLeftNum = 0;
                                            hv_maxLeftNum = 0;
                                            Variables.countLeftNum = 0;
                                            LeftTag = false;

                                        }

                                    }


                                    if (RightTag == true)
                                    {
                                        if (maxRightNum == 0)
                                        {
                                            maxRightNum = (int)hv_group2Num;

                                        }
                                        else
                                        {
                                            if ((int)hv_group2Num > maxRightNum)
                                            {
                                                maxRightNum = (int)hv_group2Num;
                                            }

                                        }

                                        hv_maxRightNum = maxRightNum;
                                        disp_message(hv_ExpDefaultWinHandle, "右通道结果层数:" + hv_maxRightNum,
                "window", 410, 800, "black", "true");

                                        if (RightTag == true && Variables.WoodIn2 == false)
                                        {
                                            Variables.countRightNum = maxRightNum;
                                            if (maxRightNum == Variables.RightStandardNum)
                                            {
                                                disp_message(hv_ExpDefaultWinHandle, "合格-右",
                "window", 460, 800, "black", "true");
                                            }
                                            else
                                            {
                                                disp_message(hv_ExpDefaultWinHandle, "不合格-右",
                "window", 460, 800, "red", "true");
                                                await Task.Run(async () => await RedLightOpen());
                                            }

                                            maxRightNum = 0;
                                            hv_maxRightNum = 0;
                                            Variables.countRightNum = 0;
                                            RightTag = false;

                                        }

                                    }


                                }
                            }
                            catch(Exception eeee)
                            {
                                MessageBox.Show("结果异常，请重启程序"+eeee.Message);
                            }




                            set_display_font(hv_ExpDefaultWinHandle, 20, "mono", "true", "false");
                            disp_message(hv_ExpDefaultWinHandle, ("图像读取耗时:" + ((((hv_T2 - hv_T1) * 1000)).TupleString(
                            ".6"))) + " ms", "window", 550, 70, "black", "true");
                        disp_message(hv_ExpDefaultWinHandle, ("预处理耗时:" + ((((hv_T3 - hv_T2) * 1000)).TupleString(
                            ".6"))) + " ms", "window", 550, 800, "black", "true");
                        disp_message(hv_ExpDefaultWinHandle, ("辨识处理耗时:" + ((((hv_T4 - hv_T3) * 1000)).TupleString(
                            ".6"))) + " ms", "window", 590, 70, "black", "true");
                        disp_message(hv_ExpDefaultWinHandle, ((("图像" + (hv_fileNo.TupleString(".5"))) + "总耗时:") + (((hv_T4 - hv_T1)).TupleString(
                            ".6"))) + " s", "window", 590, 800, "black", "true");
                        //disp_message (WindowHandle, '测试耗时:'+ ((TT2 - TT1)*1000 )$'.6' + ' ms', 'window', 140, 20, 'black', 'true')

                        HOperatorSet.SetColored(hv_ExpDefaultWinHandle, 12);
                        HOperatorSet.SetLineWidth(hv_ExpDefaultWinHandle, 3);
                        HOperatorSet.DispObj(ho_ContoursSplit, hv_ExpDefaultWinHandle);

                        hv_fileNo = hv_fileNo + 1;

                        HOperatorSet.WaitSeconds(0.1);
                    }





                        if (port.Connected)
                        {
                            WoodIn = master.ReadInputs(1, (ushort)1024, 6);
                            Variables.WoodIn1 = WoodIn[0];
                            Variables.WoodIn2 = WoodIn[1];
                            Variables.WoodIn3 = WoodIn[2];
                            Variables.WoodIn4 = WoodIn[3];
                            Variables.WoodIn5 = WoodIn[4];
                            Variables.WoodIn6 = WoodIn[5];

                        }

                        if ((Variables.WoodIn1 == false) && (Variables.WoodIn2 == false) && (Variables.WoodIn3 == false))
                        {
                            StartTag = false;

                        }


                }



            }

            }


            ho_Image.Dispose();
            ho_ImageMean.Dispose();
            ho_RegionDynThresh.Dispose();
            ho_ConnectedRegions.Dispose();
            ho_SelectedRegions.Dispose();
            ho_RegionFillUp.Dispose();
            ho_RegionFillUp1.Dispose();
            ho_Region.Dispose();
            ho_Region1.Dispose();
            ho_Skeleton.Dispose();
            ho_Contours.Dispose();
            ho_ContoursSplit.Dispose();
            ho_SelectedContours.Dispose();
            ho_SortedContours.Dispose();
            ho_ObjectSelected.Dispose();
            ho_ContCircle.Dispose();
            HOperatorSet.CloseFramegrabber(hv_AcqHandle);
        }

        public void InitHalcon()
        {
            // Default settings used in HDevelop 
            HOperatorSet.SetSystem("width", 512);
            HOperatorSet.SetSystem("height", 512);
        }

        public void RunHalcon(HTuple Window)
        {
            hv_ExpDefaultWinHandle = Window;
            action();
        }
        public async Task RedLightOpen()
        {
            await PLCControl(async master =>
            {
                byte slaveID = 1;//待修改

                for (int i = 1; i <= Variables.alarmRounds && !Variables.StopAlarm; i++)
                {
                    master.WriteSingleCoil(slaveID, 1280, true);
                    master.WriteSingleCoil(slaveID, 1281, false);
                    await Task.Delay((int)(Variables.alarmGap));
                    master.WriteSingleCoil(slaveID, 1280, false);
                    master.WriteSingleCoil(slaveID, 1281, true);
                    await Task.Delay((int)(Variables.alarmGap));
                }

                master.WriteSingleCoil(slaveID, 1280, false);
                master.WriteSingleCoil(slaveID, 1281, false);
            });
            //OPEN THE 1296-1327 PLC COIL

        }

        public async void WoodInSign()
        {
            //MessageBox.Show("光电开启");

            await PLCControl(async master =>
            {
                byte slaveID = 1;//待修改
                
                     bool[] WoodIn = master.ReadInputs(slaveID, (ushort)1024, 6);
                    Variables.WoodIn1 = WoodIn[0];
                    Variables.WoodIn2 = WoodIn[1];
                    Variables.WoodIn3 = WoodIn[2];
                    Variables.WoodIn4 = WoodIn[3];
                    Variables.WoodIn5 = WoodIn[4];
                    Variables.WoodIn6 = WoodIn[5];

                
            });


            //OPEN THE 1296-1327 PLC COIL

        }
        private async Task PLCControl(Func<IModbusMaster, Task> action)
        {
            try
            {
                using (var port = new TcpClient())
                {
                    var master = Modbus.Device.ModbusIpMaster.CreateIp(port);
                    if (port.Connected==false)
                    {
                        
                        port.Connect("192.168.1.5", 502);//待修改

                    }
                        
                    if (port.Connected)
                    {
                        if (port.SendTimeout < 200)
                        {
                            await action(master);
                        }

                    }
                    else
                    {
                        MessageBox.Show("报警灯输出受阻");
                    }
                    
                    //port.Close();
                    //MessageBox.Show("报警灯PLC通信完成");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"控制PLC出错，{ex.Message}");
            }
        }

    }
}
