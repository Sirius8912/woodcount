﻿namespace BentwoodCount
{
    partial class Preset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupDist = new System.Windows.Forms.TextBox();
            this.selectContours = new System.Windows.Forms.TextBox();
            this.MaxDist2 = new System.Windows.Forms.TextBox();
            this.MaxDist1 = new System.Windows.Forms.TextBox();
            this.segmentSooth = new System.Windows.Forms.TextBox();
            this.skeletonLength = new System.Windows.Forms.TextBox();
            this.closeCircle = new System.Windows.Forms.TextBox();
            this.threshold = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.GreenLightTestButton = new System.Windows.Forms.Button();
            this.YellowLightTestButton = new System.Windows.Forms.Button();
            this.RedLightTestButton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.alarmRounds = new System.Windows.Forms.TextBox();
            this.AlarmSeconds = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.MeanRadiusVar = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MeanRadiusVar);
            this.groupBox1.Controls.Add(this.groupDist);
            this.groupBox1.Controls.Add(this.selectContours);
            this.groupBox1.Controls.Add(this.MaxDist2);
            this.groupBox1.Controls.Add(this.MaxDist1);
            this.groupBox1.Controls.Add(this.segmentSooth);
            this.groupBox1.Controls.Add(this.skeletonLength);
            this.groupBox1.Controls.Add(this.closeCircle);
            this.groupBox1.Controls.Add(this.threshold);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 426);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "图像参数设定";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupDist
            // 
            this.groupDist.Location = new System.Drawing.Point(217, 348);
            this.groupDist.Name = "groupDist";
            this.groupDist.Size = new System.Drawing.Size(164, 30);
            this.groupDist.TabIndex = 1;
            this.groupDist.Text = "100";
            this.groupDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // selectContours
            // 
            this.selectContours.Location = new System.Drawing.Point(217, 303);
            this.selectContours.Name = "selectContours";
            this.selectContours.Size = new System.Drawing.Size(164, 30);
            this.selectContours.TabIndex = 1;
            this.selectContours.Text = "10";
            this.selectContours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxDist2
            // 
            this.MaxDist2.Location = new System.Drawing.Point(217, 258);
            this.MaxDist2.Name = "MaxDist2";
            this.MaxDist2.Size = new System.Drawing.Size(164, 30);
            this.MaxDist2.TabIndex = 1;
            this.MaxDist2.Text = "3.5";
            this.MaxDist2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxDist1
            // 
            this.MaxDist1.Location = new System.Drawing.Point(217, 213);
            this.MaxDist1.Name = "MaxDist1";
            this.MaxDist1.Size = new System.Drawing.Size(164, 30);
            this.MaxDist1.TabIndex = 1;
            this.MaxDist1.Text = "6";
            this.MaxDist1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // segmentSooth
            // 
            this.segmentSooth.Location = new System.Drawing.Point(217, 168);
            this.segmentSooth.Name = "segmentSooth";
            this.segmentSooth.Size = new System.Drawing.Size(164, 30);
            this.segmentSooth.TabIndex = 1;
            this.segmentSooth.Text = "5";
            this.segmentSooth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // skeletonLength
            // 
            this.skeletonLength.Location = new System.Drawing.Point(217, 123);
            this.skeletonLength.Name = "skeletonLength";
            this.skeletonLength.Size = new System.Drawing.Size(164, 30);
            this.skeletonLength.TabIndex = 1;
            this.skeletonLength.Text = "3";
            this.skeletonLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // closeCircle
            // 
            this.closeCircle.Location = new System.Drawing.Point(217, 78);
            this.closeCircle.Name = "closeCircle";
            this.closeCircle.Size = new System.Drawing.Size(164, 30);
            this.closeCircle.TabIndex = 1;
            this.closeCircle.Text = "5.5";
            this.closeCircle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.closeCircle.TextChanged += new System.EventHandler(this.closeCircle_TextChanged);
            // 
            // threshold
            // 
            this.threshold.Location = new System.Drawing.Point(217, 31);
            this.threshold.Name = "threshold";
            this.threshold.Size = new System.Drawing.Size(164, 30);
            this.threshold.TabIndex = 1;
            this.threshold.Text = "190";
            this.threshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.threshold.TextChanged += new System.EventHandler(this.threshold_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 351);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "分捆间最小间距：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 306);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(189, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "最小拟合元素长度：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "二次迭代回归距离：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "一次迭代回归距离：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(189, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "线条分割平滑长度：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "轴心线元素最小长度：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "膨胀圆周半径：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "切割阈值下限：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.GreenLightTestButton);
            this.groupBox2.Controls.Add(this.YellowLightTestButton);
            this.groupBox2.Controls.Add(this.RedLightTestButton);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.alarmRounds);
            this.groupBox2.Controls.Add(this.AlarmSeconds);
            this.groupBox2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(428, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(360, 342);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "报警参数设定";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(305, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "毫秒";
            // 
            // GreenLightTestButton
            // 
            this.GreenLightTestButton.Location = new System.Drawing.Point(234, 282);
            this.GreenLightTestButton.Name = "GreenLightTestButton";
            this.GreenLightTestButton.Size = new System.Drawing.Size(102, 42);
            this.GreenLightTestButton.TabIndex = 1;
            this.GreenLightTestButton.Text = "测试";
            this.GreenLightTestButton.UseVisualStyleBackColor = true;
            this.GreenLightTestButton.Click += new System.EventHandler(this.GreenLightTestButton_Click);
            // 
            // YellowLightTestButton
            // 
            this.YellowLightTestButton.Location = new System.Drawing.Point(234, 221);
            this.YellowLightTestButton.Name = "YellowLightTestButton";
            this.YellowLightTestButton.Size = new System.Drawing.Size(102, 42);
            this.YellowLightTestButton.TabIndex = 1;
            this.YellowLightTestButton.Text = "测试";
            this.YellowLightTestButton.UseVisualStyleBackColor = true;
            this.YellowLightTestButton.Click += new System.EventHandler(this.YellowLightTestButton_Click);
            // 
            // RedLightTestButton
            // 
            this.RedLightTestButton.Location = new System.Drawing.Point(234, 161);
            this.RedLightTestButton.Name = "RedLightTestButton";
            this.RedLightTestButton.Size = new System.Drawing.Size(102, 42);
            this.RedLightTestButton.TabIndex = 1;
            this.RedLightTestButton.Text = "测试";
            this.RedLightTestButton.UseVisualStyleBackColor = true;
            this.RedLightTestButton.Click += new System.EventHandler(this.RedLightTestButton_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 293);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(209, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "传送带运行绿灯测试：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 232);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(209, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "传送带停止黄灯测试：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 168);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(149, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "报警红灯测试：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(149, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "报警鸣笛轮数：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "报警鸣笛间隔：";
            // 
            // alarmRounds
            // 
            this.alarmRounds.Location = new System.Drawing.Point(181, 97);
            this.alarmRounds.Name = "alarmRounds";
            this.alarmRounds.Size = new System.Drawing.Size(118, 30);
            this.alarmRounds.TabIndex = 1;
            this.alarmRounds.Text = "4";
            this.alarmRounds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AlarmSeconds
            // 
            this.AlarmSeconds.Location = new System.Drawing.Point(181, 52);
            this.AlarmSeconds.Name = "AlarmSeconds";
            this.AlarmSeconds.Size = new System.Drawing.Size(118, 30);
            this.AlarmSeconds.TabIndex = 1;
            this.AlarmSeconds.Text = "50";
            this.AlarmSeconds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(468, 363);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 55);
            this.button1.TabIndex = 1;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(634, 363);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 55);
            this.button2.TabIndex = 1;
            this.button2.Text = "取消";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(92, 389);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(129, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "均半径偏差：";
            // 
            // MeanRadiusVar
            // 
            this.MeanRadiusVar.Location = new System.Drawing.Point(217, 386);
            this.MeanRadiusVar.Name = "MeanRadiusVar";
            this.MeanRadiusVar.Size = new System.Drawing.Size(164, 30);
            this.MeanRadiusVar.TabIndex = 2;
            this.MeanRadiusVar.Text = "150";
            this.MeanRadiusVar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Preset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Preset";
            this.Text = "Preset";
            this.Load += new System.EventHandler(this.Preset_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox groupDist;
        private System.Windows.Forms.TextBox selectContours;
        private System.Windows.Forms.TextBox MaxDist2;
        private System.Windows.Forms.TextBox MaxDist1;
        private System.Windows.Forms.TextBox segmentSooth;
        private System.Windows.Forms.TextBox skeletonLength;
        private System.Windows.Forms.TextBox closeCircle;
        private System.Windows.Forms.TextBox threshold;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button GreenLightTestButton;
        private System.Windows.Forms.Button YellowLightTestButton;
        private System.Windows.Forms.Button RedLightTestButton;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox AlarmSeconds;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox alarmRounds;
        private System.Windows.Forms.TextBox MeanRadiusVar;
        private System.Windows.Forms.Label label15;
    }
}