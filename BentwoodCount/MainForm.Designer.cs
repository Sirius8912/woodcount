﻿namespace BentwoodCount
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.hWindowControl1 = new HalconDotNet.HWindowControl();
            this.StartRecogButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.BeltButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RightNumLabel = new System.Windows.Forms.Label();
            this.LeftNumLabel = new System.Windows.Forms.Label();
            this.StandNumResult = new System.Windows.Forms.Label();
            this.StandGood = new System.Windows.Forms.Label();
            this.RightGood = new System.Windows.Forms.Label();
            this.LeftGood = new System.Windows.Forms.Label();
            this.StandLabel2 = new System.Windows.Forms.Label();
            this.RightLabel = new System.Windows.Forms.Label();
            this.LeftLabel = new System.Windows.Forms.Label();
            this.StandLabel = new System.Windows.Forms.Label();
            this.StandNumLabel = new System.Windows.Forms.Label();
            this.StandNumInput = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.MultiChannelCheckBox = new System.Windows.Forms.CheckBox();
            this.LeftStandInput = new System.Windows.Forms.TextBox();
            this.rightStandInput = new System.Windows.Forms.TextBox();
            this.rollBack = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // hWindowControl1
            // 
            this.hWindowControl1.BackColor = System.Drawing.Color.Black;
            this.hWindowControl1.BorderColor = System.Drawing.Color.Black;
            this.hWindowControl1.ImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindowControl1.Location = new System.Drawing.Point(12, 12);
            this.hWindowControl1.Name = "hWindowControl1";
            this.hWindowControl1.Size = new System.Drawing.Size(1777, 786);
            this.hWindowControl1.TabIndex = 0;
            this.hWindowControl1.WindowSize = new System.Drawing.Size(1777, 786);
            // 
            // StartRecogButton
            // 
            this.StartRecogButton.Location = new System.Drawing.Point(1427, 833);
            this.StartRecogButton.Name = "StartRecogButton";
            this.StartRecogButton.Size = new System.Drawing.Size(148, 44);
            this.StartRecogButton.TabIndex = 1;
            this.StartRecogButton.Text = "开始识别";
            this.StartRecogButton.UseVisualStyleBackColor = true;
            this.StartRecogButton.Click += new System.EventHandler(this.StartCount);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1614, 832);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "设置参数";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BeltButton
            // 
            this.BeltButton.Location = new System.Drawing.Point(1225, 834);
            this.BeltButton.Name = "BeltButton";
            this.BeltButton.Size = new System.Drawing.Size(148, 44);
            this.BeltButton.TabIndex = 1;
            this.BeltButton.Text = "启动传送带";
            this.BeltButton.UseVisualStyleBackColor = true;
            this.BeltButton.Click += new System.EventHandler(this.BeltButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RightNumLabel);
            this.groupBox1.Controls.Add(this.LeftNumLabel);
            this.groupBox1.Controls.Add(this.StandNumResult);
            this.groupBox1.Controls.Add(this.StandGood);
            this.groupBox1.Controls.Add(this.RightGood);
            this.groupBox1.Controls.Add(this.LeftGood);
            this.groupBox1.Controls.Add(this.StandLabel2);
            this.groupBox1.Controls.Add(this.RightLabel);
            this.groupBox1.Controls.Add(this.LeftLabel);
            this.groupBox1.Controls.Add(this.StandLabel);
            this.groupBox1.Font = new System.Drawing.Font("隶书", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(1416, 299);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 185);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "识别结果";
            // 
            // RightNumLabel
            // 
            this.RightNumLabel.AutoSize = true;
            this.RightNumLabel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.RightNumLabel.Location = new System.Drawing.Point(213, 149);
            this.RightNumLabel.Name = "RightNumLabel";
            this.RightNumLabel.Size = new System.Drawing.Size(50, 27);
            this.RightNumLabel.TabIndex = 2;
            this.RightNumLabel.Text = "N.A";
            this.RightNumLabel.Visible = false;
            // 
            // LeftNumLabel
            // 
            this.LeftNumLabel.AutoSize = true;
            this.LeftNumLabel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LeftNumLabel.Location = new System.Drawing.Point(42, 149);
            this.LeftNumLabel.Name = "LeftNumLabel";
            this.LeftNumLabel.Size = new System.Drawing.Size(50, 27);
            this.LeftNumLabel.TabIndex = 2;
            this.LeftNumLabel.Text = "N.A";
            this.LeftNumLabel.Visible = false;
            // 
            // StandNumResult
            // 
            this.StandNumResult.AutoSize = true;
            this.StandNumResult.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StandNumResult.Location = new System.Drawing.Point(158, 19);
            this.StandNumResult.Name = "StandNumResult";
            this.StandNumResult.Size = new System.Drawing.Size(50, 27);
            this.StandNumResult.TabIndex = 2;
            this.StandNumResult.Text = "N.A";
            this.StandNumResult.Click += new System.EventHandler(this.StandNumResult_Click);
            // 
            // StandGood
            // 
            this.StandGood.AutoSize = true;
            this.StandGood.Location = new System.Drawing.Point(131, 58);
            this.StandGood.Name = "StandGood";
            this.StandGood.Size = new System.Drawing.Size(49, 20);
            this.StandGood.TabIndex = 1;
            this.StandGood.Text = "合格";
            // 
            // RightGood
            // 
            this.RightGood.AutoSize = true;
            this.RightGood.Location = new System.Drawing.Point(214, 83);
            this.RightGood.Name = "RightGood";
            this.RightGood.Size = new System.Drawing.Size(49, 20);
            this.RightGood.TabIndex = 1;
            this.RightGood.Text = "合格";
            this.RightGood.Visible = false;
            // 
            // LeftGood
            // 
            this.LeftGood.AutoSize = true;
            this.LeftGood.Location = new System.Drawing.Point(43, 83);
            this.LeftGood.Name = "LeftGood";
            this.LeftGood.Size = new System.Drawing.Size(49, 20);
            this.LeftGood.TabIndex = 1;
            this.LeftGood.Text = "合格";
            this.LeftGood.Visible = false;
            // 
            // StandLabel2
            // 
            this.StandLabel2.AutoSize = true;
            this.StandLabel2.Location = new System.Drawing.Point(214, 26);
            this.StandLabel2.Name = "StandLabel2";
            this.StandLabel2.Size = new System.Drawing.Size(29, 20);
            this.StandLabel2.TabIndex = 0;
            this.StandLabel2.Text = "个";
            // 
            // RightLabel
            // 
            this.RightLabel.AutoSize = true;
            this.RightLabel.Location = new System.Drawing.Point(175, 118);
            this.RightLabel.Name = "RightLabel";
            this.RightLabel.Size = new System.Drawing.Size(129, 20);
            this.RightLabel.TabIndex = 0;
            this.RightLabel.Text = "右通道个数：";
            this.RightLabel.Visible = false;
            // 
            // LeftLabel
            // 
            this.LeftLabel.AutoSize = true;
            this.LeftLabel.Location = new System.Drawing.Point(7, 118);
            this.LeftLabel.Name = "LeftLabel";
            this.LeftLabel.Size = new System.Drawing.Size(129, 20);
            this.LeftLabel.TabIndex = 0;
            this.LeftLabel.Text = "左通道个数：";
            this.LeftLabel.Visible = false;
            // 
            // StandLabel
            // 
            this.StandLabel.AutoSize = true;
            this.StandLabel.Location = new System.Drawing.Point(63, 26);
            this.StandLabel.Name = "StandLabel";
            this.StandLabel.Size = new System.Drawing.Size(109, 20);
            this.StandLabel.TabIndex = 0;
            this.StandLabel.Text = "木板个数：";
            // 
            // StandNumLabel
            // 
            this.StandNumLabel.AutoSize = true;
            this.StandNumLabel.Font = new System.Drawing.Font("楷体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StandNumLabel.Location = new System.Drawing.Point(26, 845);
            this.StandNumLabel.Name = "StandNumLabel";
            this.StandNumLabel.Size = new System.Drawing.Size(142, 25);
            this.StandNumLabel.TabIndex = 3;
            this.StandNumLabel.Text = "标准个数：";
            // 
            // StandNumInput
            // 
            this.StandNumInput.Location = new System.Drawing.Point(174, 845);
            this.StandNumInput.Name = "StandNumInput";
            this.StandNumInput.Size = new System.Drawing.Size(99, 25);
            this.StandNumInput.TabIndex = 4;
            this.StandNumInput.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1500, 490);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(159, 44);
            this.button4.TabIndex = 1;
            this.button4.Text = "清零统计";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(1416, 553);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 217);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "质量管理统计";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(193, 165);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 27);
            this.label10.TabIndex = 2;
            this.label10.Text = "N.A";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(193, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 27);
            this.label9.TabIndex = 2;
            this.label9.Text = "N.A";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(193, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 27);
            this.label3.TabIndex = 2;
            this.label3.Text = "N.A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "捆";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(193, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 27);
            this.label5.TabIndex = 2;
            this.label5.Text = "N.A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(265, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "捆";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(91, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "出错率：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "正品率：";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "出错木板捆数：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(72, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "已木板捆数：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("楷体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(491, 845);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 25);
            this.label12.TabIndex = 3;
            this.label12.Text = "左通道个数：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("楷体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(788, 845);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(168, 25);
            this.label13.TabIndex = 3;
            this.label13.Text = "右通道个数：";
            // 
            // MultiChannelCheckBox
            // 
            this.MultiChannelCheckBox.AutoSize = true;
            this.MultiChannelCheckBox.Location = new System.Drawing.Point(381, 846);
            this.MultiChannelCheckBox.Name = "MultiChannelCheckBox";
            this.MultiChannelCheckBox.Size = new System.Drawing.Size(89, 19);
            this.MultiChannelCheckBox.TabIndex = 5;
            this.MultiChannelCheckBox.Text = "双捆模式";
            this.MultiChannelCheckBox.UseVisualStyleBackColor = true;
            this.MultiChannelCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // LeftStandInput
            // 
            this.LeftStandInput.Enabled = false;
            this.LeftStandInput.Location = new System.Drawing.Point(655, 845);
            this.LeftStandInput.Name = "LeftStandInput";
            this.LeftStandInput.Size = new System.Drawing.Size(100, 25);
            this.LeftStandInput.TabIndex = 6;
            this.LeftStandInput.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // rightStandInput
            // 
            this.rightStandInput.Enabled = false;
            this.rightStandInput.Location = new System.Drawing.Point(953, 845);
            this.rightStandInput.Name = "rightStandInput";
            this.rightStandInput.Size = new System.Drawing.Size(100, 25);
            this.rightStandInput.TabIndex = 6;
            this.rightStandInput.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // rollBack
            // 
            this.rollBack.AutoSize = true;
            this.rollBack.Location = new System.Drawing.Point(1116, 847);
            this.rollBack.Name = "rollBack";
            this.rollBack.Size = new System.Drawing.Size(59, 19);
            this.rollBack.TabIndex = 7;
            this.rollBack.Text = "反转";
            this.rollBack.UseVisualStyleBackColor = true;
            this.rollBack.CheckedChanged += new System.EventHandler(this.rollBack_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(31, 816);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(277, 79);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "单捆模式";
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(350, 816);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(712, 79);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "双捆模式";
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(1096, 816);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(293, 79);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "传送带控制";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(1435, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(345, 20);
            this.label11.TabIndex = 9;
            this.label11.Text = "福建铂格智能科技股份公司版权所有";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1801, 925);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.rollBack);
            this.Controls.Add(this.rightStandInput);
            this.Controls.Add(this.LeftStandInput);
            this.Controls.Add(this.MultiChannelCheckBox);
            this.Controls.Add(this.StandNumInput);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.StandNumLabel);
            this.Controls.Add(this.BeltButton);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.StartRecogButton);
            this.Controls.Add(this.hWindowControl1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Name = "MainForm";
            this.Text = "曲木板个数自动检测系统——铂格科技";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private HalconDotNet.HWindowControl hWindowControl1;
        private System.Windows.Forms.Button StartRecogButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BeltButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LeftGood;
        private System.Windows.Forms.Label StandLabel2;
        private System.Windows.Forms.Label StandLabel;
        private System.Windows.Forms.Label StandNumLabel;
        private System.Windows.Forms.TextBox StandNumInput;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label RightNumLabel;
        private System.Windows.Forms.Label LeftNumLabel;
        private System.Windows.Forms.Label StandGood;
        private System.Windows.Forms.Label RightGood;
        private System.Windows.Forms.Label RightLabel;
        private System.Windows.Forms.Label LeftLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox MultiChannelCheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LeftStandInput;
        private System.Windows.Forms.TextBox rightStandInput;
        public System.Windows.Forms.Label StandNumResult;
        private System.Windows.Forms.CheckBox rollBack;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label11;
    }
}

