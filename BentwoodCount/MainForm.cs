﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Net.Sockets;
using Modbus;
using Modbus.Device;
using System.Threading;
using System.Security.Cryptography;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Security.Cryptography;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Diagnostics;



namespace BentwoodCount
{


    public partial class MainForm : Form
    {
        bool startRecog=false;
        bool startBelt = false;
        public MainForm()
        {
            InitializeComponent();
        }

        private void PLCControl(Action<IModbusMaster> action)
        {
            try
            {
                using (var port = new TcpClient())
                {
                    var master = Modbus.Device.ModbusIpMaster.CreateIp(port);

                    port.Connect("192.168.1.5", 502);//待修改
                    action(master);
                    port.Close();
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show($"控制PLC出错，{ex.Message}");
            }
        }




        public void YellowLightOpen()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改


                master.WriteSingleCoil(slaveID, 1282, true);


            });
            //OPEN THE 1296-1327 PLC COIL

        }

        public void YellowLightClose()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改


                master.WriteSingleCoil(slaveID, 1282, false);


            });
            //OPEN THE 1296-1327 PLC COIL

        }

        public void GreenLightClose()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改


                master.WriteSingleCoil(slaveID, 1283, false);


            });
            //OPEN THE 1296-1327 PLC COIL

        }


        public void GreenLightOpen()
        {
            PLCControl(master =>
            {
                byte slaveID = 1;//待修改


                master.WriteSingleCoil(slaveID, 1283, true);


            });
            //OPEN THE 1296-1327 PLC COIL

        }

        Thread CountGo;
        Thread CountNumThread;
        Thread CountLeftNumThread;
        Thread CountRightNumThread;
        


        public void WoodInSign()
        {
            //MessageBox.Show("光电开启");

                PLCControl(master =>
                {
                    byte slaveID = 1;//待修改
                    while (true)
                    {
                    bool[] WoodIn = master.ReadInputs(slaveID, (ushort)1024, 6);
                    Variables.WoodIn1 = WoodIn[0];
                    Variables.WoodIn2 = WoodIn[1];
                    Variables.WoodIn3 = WoodIn[2];
                    Variables.WoodIn4 = WoodIn[3];
                    Variables.WoodIn5 = WoodIn[4];
                    Variables.WoodIn6 = WoodIn[5];
                        
                    }
                });


            //OPEN THE 1296-1327 PLC COIL

        }
        private void CountNum()
        {
            int temp=1000;
            try
            {
                unsafe
                {
                    fixed(int* StandNum = & Variables.countNum)
                    {
                        for (int i = 0; i < 10; i++)
                            temp = *StandNum;
                    //MessageBox.Show("主界面"+ temp.ToString());
                    //StandNumResult.Text = temp.ToString();
                    }
                    

                }


            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            

            

        }

        private void CountLeftNum()
        {
            this.InvokeIfRequired(new Action(() =>
            {
                LeftNumLabel.Text = Variables.countLeftNum.ToString();
            }));
            

        }

        private void CountRightNum()
        {
            this.InvokeIfRequired(new Action(() =>
            {
                RightNumLabel.Text = Variables.countRightNum.ToString();
            }));
            
        }

        public bool RecogOnGoing=false;

        private void StartCount(object sender, EventArgs e)
            
        {

            if (TimeClass.GetNowDate() == "20190307")
            {

                string str1 = "0";
                File.WriteAllText("system.txt", str1);
            }
            else
            {
                string str = File.ReadAllText("system.txt");
                int i = int.Parse(str);
                i++;
                string str1 = i.ToString();
                File.WriteAllText("system.txt", str1);
                
                
            }
            int res = TimeClass.InitRegedit();



            if (res == 0)
            {
                if (Variables.Multi == false && (StandNumInput.Text == "0" || StandNumInput.Text == ""))
                {
                    MessageBox.Show("请输入每捆的标准个数！");

                }

                else
                {
                    if (Variables.Multi == true && (LeftStandInput.Text == "0" || LeftStandInput.Text == "") && (LeftStandInput.Text == "0" || LeftStandInput.Text == ""))
                    {
                        MessageBox.Show("请输入左右通道的标准个数！");
                    }
                    else
                    {
                        if (string.Equals(StartRecogButton.Text, "开始识别"))
                        {
                            if (startRecog == false)
                            {

                                try
                                {
                                    startRecog = true;
                                    RecogOnGoing = true;


                                    //CountNumThread = new Thread(CountNum);
                                    //CountNumThread.Start();
                                    //CountNumThread.IsBackground = true;

                                    CountGo = new Thread(CountFunc);

                                    CountGo.Start();

                                    CountGo.IsBackground = true;
                                    //result = BeginInvoke(CountTool);
                                    StartRecogButton.Text = "停止识别";
                                    if (MultiChannelCheckBox.Checked == false)
                                    {
                                        StandNumInput.Enabled = false;
                                    }
                                    else
                                    {
                                        LeftStandInput.Enabled = false;
                                        rightStandInput.Enabled = false;
                                    }

                                }
                                catch (Exception ee)
                                {
                                    MessageBox.Show(ee.Message);
                                }



                            }
                            else
                            {
                                try
                                {

                                RecogOnGoing = true;
                                    Variables.pause = false;
                                StartRecogButton.Text = "停止识别";
                                    if (MultiChannelCheckBox.Checked == false)
                                    {
                                        StandNumInput.Enabled = false;
                                    }
                                    else
                                    {
                                        LeftStandInput.Enabled = false;
                                        rightStandInput.Enabled = false;
                                    }
                                    //WoodSignInThread.Resume();
                                }
                                catch (Exception ee)
                                {
                                    MessageBox.Show("resume():"+ee.Message);
                                }


                            }

                        }

                        else
                        {
                            try
                            {
                                RecogOnGoing = false;
                                StartRecogButton.Text = "开始识别";
                                Variables.pause = true;
                                //WoodSignInThread.Suspend();
                                if (MultiChannelCheckBox.Checked == false)
                                {
                                    StandNumInput.Enabled = true;
                                }
                                else
                                {
                                    LeftStandInput.Enabled = true;
                                    rightStandInput.Enabled = true;
                                }
                            }
                            catch (Exception ee)
                            {
                                MessageBox.Show("suspend():" + ee.Message);
                            }

                        }
                    }
                }
            }
            else if (res == 1)
            {
                MessageBox.Show("软件尚未注册，请注册软件！");
            }
            else if (res == 2)
            {
                MessageBox.Show("注册机器与本机不一致,请联系管理员！");
            }
            else if (res == 3)
            {
                MessageBox.Show("软件试用已到期！");
            }
            else
            {
                MessageBox.Show("软件运行出错，请重新启动！");
            }

 

            //MethodInvoker CountTool = new MethodInvoker(CountFunc);
            //IAsyncResult result = null;




        }

        private void CountFunc()
        {
            HDevelopExport WoodCount = new HDevelopExport();
            WoodCount.RunHalcon(hWindowControl1.HalconWindow);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Variables.StandardNum = int.Parse(StandNumInput.Text.ToString());
            }catch(Exception eee)
            {
                MessageBox.Show(eee.Message+"\n提示:请注意键盘上的Numlock灯是否亮着，如果是，按一下Numlock按键！");
            }
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Preset().ShowDialog();
        }

        private void BeltButton_Click(object sender, EventArgs e)
        {

            
            if(Variables.rollback==true)
            {
                if (BeltButton.Text == "启动传送带")
                {
                    Thread BeltRollbackFunc = new Thread(BeltRollback);
                    Thread GreenOpenThread = new Thread(GreenLightOpen);
                    Thread YellowCloseThread = new Thread(YellowLightClose);
                    BeltButton.Text = "停止传送带";
                    BeltRollbackFunc.Start();

                    BeltRollbackFunc.IsBackground = true;

                    GreenOpenThread.Start();
                    GreenOpenThread.IsBackground = true;
                    YellowCloseThread.Start();
                    YellowCloseThread.IsBackground = true;




                }
                else
                {
                    Thread BeltStopFunc = new Thread(BeltStop);
                    Thread GreenCloseThread = new Thread(GreenLightClose);
                    Thread YellowOpenThread = new Thread(YellowLightOpen);
                    BeltButton.Text = "启动传送带";
                    BeltStopFunc.Start();
                    BeltStopFunc.IsBackground = true;
                    GreenCloseThread.Start();
                    GreenCloseThread.IsBackground = true;
                    YellowOpenThread.Start();
                    YellowOpenThread.IsBackground = true;

                }
            }
            else
            {
                if (BeltButton.Text == "启动传送带")
                {
                    Thread BeltOnFunc = new Thread(BeltOn);
                    Thread GreenOpenThread = new Thread(GreenLightOpen);
                    Thread YellowCloseThread = new Thread(YellowLightClose);
                    BeltButton.Text = "停止传送带";
                    BeltOnFunc.Start();

                    BeltOnFunc.IsBackground = true;

                    GreenOpenThread.Start();
                    GreenOpenThread.IsBackground = true;
                    YellowCloseThread.Start();
                    YellowCloseThread.IsBackground = true;




                }
                else
                {
                    Thread BeltStopFunc = new Thread(BeltStop);
                    Thread GreenCloseThread = new Thread(GreenLightClose);
                    Thread YellowOpenThread = new Thread(YellowLightOpen);
                    BeltButton.Text = "启动传送带";
                    BeltStopFunc.Start();
                    BeltStopFunc.IsBackground = true;
                    GreenCloseThread.Start();
                    GreenCloseThread.IsBackground = true;
                    YellowOpenThread.Start();
                    YellowOpenThread.IsBackground = true;

                }
            }
            



        }

        private object mSerialPortLock = new object();
        private const string PORT_NAME = "COM2";

        private void BeltOn()
        {
            try
            {
                lock (mSerialPortLock)
                {
                    using (SerialPort serialPort1 = new SerialPort(PORT_NAME))
                    {
                        IModbusSerialMaster master = ModbusSerialMaster.CreateRtu(serialPort1);
                        serialPort1.BaudRate = 9600;
                        serialPort1.DataBits = 8;
                        serialPort1.Parity = Parity.None;
                        serialPort1.StopBits = StopBits.One;
                        serialPort1.Open();
                        byte slaveID = 1;
                        ushort registerAddress = 8192;
                        ushort value = 1;//你要写的值
                        master.WriteSingleRegister(slaveID, registerAddress, value);

                        serialPort1.Close();

                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }



        }

        private void BeltRollback()
        {
            try
            {
                lock (mSerialPortLock)
                {
                    using (SerialPort serialPort1 = new SerialPort(PORT_NAME))
                    {
                        IModbusSerialMaster master = ModbusSerialMaster.CreateRtu(serialPort1);
                        serialPort1.BaudRate = 9600;
                        serialPort1.DataBits = 8;
                        serialPort1.Parity = Parity.None;
                        serialPort1.StopBits = StopBits.One;
                        serialPort1.Open();
                        byte slaveID = 1;
                        ushort registerAddress = 8192;
                        ushort value = 2;//你要写的值
                        master.WriteSingleRegister(slaveID, registerAddress, value);

                        serialPort1.Close();

                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }



        }
        private void BeltStop()
        {
            try
            {
                lock (mSerialPortLock)
                {
                    using (SerialPort serialPort1 = new SerialPort(PORT_NAME))
                    {
                        IModbusSerialMaster master = ModbusSerialMaster.CreateRtu(serialPort1);
                        serialPort1.BaudRate = 9600;
                        serialPort1.DataBits = 8;
                        serialPort1.Parity = Parity.None;
                        serialPort1.StopBits = StopBits.One;
                        serialPort1.Open();
                        byte slaveID = 1;
                        ushort registerAddress = 8192;
                        ushort value = 6;//你要写的值
                        master.WriteSingleRegister(slaveID, registerAddress, value);
                        serialPort1.Close();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }


        private void BeltControl(Action<IModbusMaster> action)
        {
            try
            {
                lock (mSerialPortLock)
                {
                    using (SerialPort port = new SerialPort(PORT_NAME))
                    {
                        port.BaudRate = 9600;
                        port.DataBits = 8;
                        port.Parity = Parity.None;
                        port.StopBits = StopBits.One;
                        var master = Modbus.Device.ModbusSerialMaster.CreateRtu(port);
                        port.Open();
                        action(master);
                        port.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"控制传送带出错，{ex.Message}");
            }
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(MultiChannelCheckBox.Checked==true)
            {
                StandNumInput.Enabled = false;
                StandLabel.Visible = false;
                StandLabel2.Visible = false;
                StandGood.Visible = false;
                StandNumResult.Visible = false;
                LeftStandInput.Enabled = true;
                rightStandInput.Enabled = true;
                LeftLabel.Visible = true;
                LeftGood.Visible = true;
                LeftNumLabel.Visible = true;
                RightLabel.Visible = true;
                RightGood.Visible = true;
                RightNumLabel.Visible = true;
                Variables.Multi = true;
            }
            else
            {
                StandNumInput.Enabled = true;
                StandLabel.Visible = true;
                StandLabel2.Visible = true;
                StandGood.Visible = true;
                StandNumResult.Visible = true;
                LeftStandInput.Enabled = false;
                rightStandInput.Enabled = false;
                LeftLabel.Visible = false;
                LeftGood.Visible = false;
                LeftNumLabel.Visible = false;
                RightLabel.Visible = false;
                RightGood.Visible = false;
                RightNumLabel.Visible = false;
                Variables.Multi = false;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Variables.StopAlarm = true;
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            try
            { 

            Variables.LeftStandardNum = int.Parse(LeftStandInput.Text);
        }catch(Exception eee)
            {
                MessageBox.Show(eee.Message+"\n提示:请注意键盘上的Numlock灯是否亮着，如果是，按一下Numlock按键！");
            }

}

private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {

            
            Variables.RightStandardNum = int.Parse(rightStandInput.Text);
        }catch(Exception eee)
            {
                MessageBox.Show(eee.Message+"\n提示:请注意键盘上的Numlock灯是否亮着，如果是，按一下Numlock按键！");
            }

}

private void StandNumResult_Click(object sender, EventArgs e)
        {

        }

        private void rollBack_CheckedChanged(object sender, EventArgs e)
        {
            if (rollBack.Checked == true)
                Variables.rollback = true;
            else
                Variables.rollback = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;     //设置窗体为无边框样式
            this.WindowState = FormWindowState.Maximized;
            Variables.threshold=double.Parse(File.ReadAllText("Para (1).txt"));
            Variables.closeCircle = double.Parse(File.ReadAllText("Para (2).txt"));
            Variables.skeletonLength = double.Parse(File.ReadAllText("Para (3).txt"));
            Variables.segmentSmooth = double.Parse(File.ReadAllText("Para (4).txt"));
            Variables.maxDist1 = double.Parse(File.ReadAllText("Para (5).txt"));
            Variables.maxDist2 = double.Parse(File.ReadAllText("Para (6).txt"));
            Variables.selectContours = double.Parse(File.ReadAllText("Para (7).txt"));
            Variables.groupDist = double.Parse(File.ReadAllText("Para (8).txt"));
            Variables.MeanRadiusVar = double.Parse(File.ReadAllText("Para (9).txt"));
            Variables.alarmGap = int.Parse(File.ReadAllText("Para (10).txt"));
            Variables.alarmRounds = int.Parse(File.ReadAllText("Para (11).txt"));

            //SysHook h = new SysHook();
            //h.Hook_Start();
        }
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Alt) || (e.KeyCode == Keys.Control))
            {
                MessageBox.Show("危险操作，参数作废，请重启机器！");
                e.Handled = true;
            }
        }

    }

 

        class SysHook
        {
            [DllImport("user32")]
            public static extern bool BlockInput(bool isBlock);
            [DllImport(@"native.dll", EntryPoint = "FuckSysKey")]
            private extern static bool FuckSysKey(bool enAble);

            public void BlockKeyAndMouse(bool b)
            {
                BlockInput(b);
                FuckSysKey(b);//锁定ctrl+alt+del
            }
            public delegate int HookProc(int nCode, int wParam, IntPtr lParam);
            static int hHook = 0;
            public const int WH_KEYBOARD_LL = 13;

            //LowLevel键盘截获，如果是WH_KEYBOARD＝2，并不能对系统键盘截取，Acrobat Reader会在你截取之前获得键盘。 
            HookProc KeyBoardHookProcedure;

            //键盘Hook结构函数 
            [StructLayout(LayoutKind.Sequential)]
            public class KeyBoardHookStruct
            {
                public int vkCode;
                public int scanCode;
                public int flags;
                public int time;
                public int dwExtraInfo;
            }

            #region DllImport
            //设置钩子 
            [DllImport("user32.dll")]
            public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);
            [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
            //抽掉钩子 
            public static extern bool UnhookWindowsHookEx(int idHook);
            [DllImport("user32.dll")]
            //调用下一个钩子 
            public static extern int CallNextHookEx(int idHook, int nCode, int wParam, IntPtr lParam);

            [DllImport("kernel32.dll")]
            public static extern int GetCurrentThreadId();

            [DllImport("kernel32.dll")]
            public static extern IntPtr GetModuleHandle(string name);

            public void Hook_Start()
            {
                // 安装键盘钩子 
                if (hHook == 0)
                {
                    KeyBoardHookProcedure = new HookProc(KeyBoardHookProc);

                    //hHook = SetWindowsHookEx(2, 
                    //            KeyBoardHookProcedure, 
                    //          GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName), GetCurrentThreadId());

                    hHook = SetWindowsHookEx(WH_KEYBOARD_LL,
                              KeyBoardHookProcedure,
                            GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName), 0);

                    //如果设置钩子失败. 
                    if (hHook == 0)
                    {
                        Hook_Clear();

                        //throw new Exception("设置Hook失败!"); 
                    }
                }
            }

            //取消钩子事件 
            public void Hook_Clear()
            {
                bool retKeyboard = true;
                if (hHook != 0)
                {
                    retKeyboard = UnhookWindowsHookEx(hHook);
                    hHook = 0;
                }
                //如果去掉钩子失败. 
                if (!retKeyboard) throw new Exception("UnhookWindowsHookEx failed.");

            }

            //这里可以添加自己想要的信息处理 
            public static int KeyBoardHookProc(int nCode, int wParam, IntPtr lParam)
            {
                if (nCode >= 0)
                {
                    KeyBoardHookStruct kbh = (KeyBoardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyBoardHookStruct));
                    // MessageBox.Show(kbh.vkCode.ToString());

                    if (kbh.vkCode != 32)
                    {
                        return 1;
                    }
                }
                return CallNextHookEx(hHook, nCode, wParam, lParam);
            }

            #endregion

        }
    


    public class Encryption
    {
        public static string EncryPW(string Pass, string Key)
        {
            return DesEncrypt(Pass, Key);
        }

        public static string DisEncryPW(string strPass, string Key)
        {
            return DesDecrypt(strPass, Key);
        }

        /////////////////////////////////////////////////////////////////////   

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="encryptString"></param>
        /// <returns></returns>
        public static string DesEncrypt(string encryptString, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key.Substring(0, 8));
            byte[] keyIV = keyBytes;
            byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);
            
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, provider.CreateEncryptor(keyBytes, keyIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="decryptString"></param>
        /// <returns></returns>
        public static string DesDecrypt(string decryptString, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key.Substring(0, 8));
            byte[] keyIV = keyBytes;
            byte[] inputByteArray = Convert.FromBase64String(decryptString);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, provider.CreateDecryptor(keyBytes, keyIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Encoding.UTF8.GetString(mStream.ToArray());
        }



        //////////////////////////////////////////////////////
    }




    class TimeClass
    {
        public static int InitRegedit()
        {
            while (false)
            {
                /*检查注册表*/
                string SericalNumber = ReadSetting("", "SerialNumber", "-1");    // 读取注册表， 检查是否注册 -1为未注册
                if (SericalNumber == "-1")
                {
                    return 1;
                }

                /* 比较CPUid */
                string CpuId = GetSoftEndDateAllCpuId(1, SericalNumber);   //从注册表读取CPUid
                string CpuIdThis = GetCpuId();           //获取本机CPUId         
                if (CpuId != CpuIdThis)
                {
                    return 2;
                }

            }

            /* 比较时间 */
            string NowDate = TimeClass.GetNowDate();
            string EndDate = "20190415";
            string str = File.ReadAllText("system.txt");
            int times = int.Parse(str);
            if (Convert.ToInt32(EndDate) - Convert.ToInt32(NowDate) < 0||times>600)
            {
                return 3;
            }

            return 0;
        }


        /*CPUid*/
        public static string GetCpuId()
        {
            ManagementClass mc = new ManagementClass("Win32_Processor");
            ManagementObjectCollection moc = mc.GetInstances();

            string strCpuID = null;
            foreach (ManagementObject mo in moc)
            {
                strCpuID = mo.Properties["ProcessorId"].Value.ToString();
                break;
            }
            return strCpuID;
        }

        /*当前时间*/
        public static string GetNowDate()
        {
            string NowDate = DateTime.Now.ToString("yyyyMMdd"); //.Year + DateTime.Now.Month + DateTime.Now.Day).ToString();

            //     DateTime date = Convert.ToDateTime(NowDate, "yyyy/MM/dd");
            return NowDate;
        }

        /* 生成序列号 */
        public static string CreatSerialNumber()
        {
            string SerialNumber = GetCpuId() + "-" + "20190430";
            return SerialNumber;
        }

        /* 
         * i=1 得到 CUP 的id 
         * i=0 得到上次或者 开始时间 
         */
        public static string GetSoftEndDateAllCpuId(int i, string SerialNumber)
        {
            if (i == 1)
            {
                string cupId = SerialNumber.Substring(0, SerialNumber.LastIndexOf("-")); // .LastIndexOf("-"));

                return cupId;
            }
            if (i == 0)
            {
                string dateTime = SerialNumber.Substring(SerialNumber.LastIndexOf("-") + 1);
                //  dateTime = dateTime.Insert(4, "/").Insert(7, "/");
                //  DateTime date = Convert.ToDateTime(dateTime);

                return dateTime;
            }
            else
            {
                return string.Empty;
            }
        }

        /*写入注册表*/
        public static void WriteSetting(string Section, string Key, string Setting)  // name = key  value=setting  Section= path
        {
            string text1 = Section;
            RegistryKey key1 = Registry.CurrentUser.CreateSubKey("Software\\MyTest_ChildPlat\\ChildPlat"); // .LocalMachine.CreateSubKey("Software\\mytest");
            if (key1 == null)
            {
                return;
            }
            try
            {
                key1.SetValue(Key, Setting);
            }
            catch (Exception exception1)
            {
                return;
            }
            finally
            {
                key1.Close();
            }

        }

        /*读取注册表*/
        public static string ReadSetting(string Section, string Key, string Default)
        {
            if (Default == null)
            {
                Default = "-1";
            }
            string text2 = Section;
            RegistryKey key1 = Registry.CurrentUser.OpenSubKey("Software\\MyTest_ChildPlat\\ChildPlat");
            if (key1 != null)
            {
                object obj1 = key1.GetValue(Key, Default);
                key1.Close();
                if (obj1 != null)
                {
                    if (!(obj1 is string))
                    {
                        return "-1";
                    }
                    string obj2 = obj1.ToString();
                    obj2 = Encryption.DisEncryPW(obj2, "ejiang11");
                    return obj2;
                }
                return "-1";
            }


            return Default;
        }
    }



}



